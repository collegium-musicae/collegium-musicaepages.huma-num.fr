# Programme du stage

#### Lundi 17 juin
- 10h-12h Visite de l'Institut de recherche et coordination acoustique/musique (L'IRCAM)
- Déjeuner à l’institut Jean Le Rond ∂’Alembert
- 14h-17h30 Après-midi avec l’équipe Lutheries - Acoustique - Musique (LAM) de l'institut Jean Le Rond ∂’Alembert et Jean-Loïc Le Carrou

#### Mardi 18 juin
- 9h30-16h30 Journée à l’Institut de Recherche en Musicologie (IReMus). Présentation des projets de stage et répartition des groupes de travail

#### Mercredi 19 juin
- 9h30-12h Atelier de programmation informatique appliquée à la musique avec Antoine Lebrun
- Déjeuner à l’IReMus 
- 14h-17h30 Présentation d'outils graphiques (Canva) pour la réalisation d'un poster scientifique avec Félix Poullet-Pagès

#### Jeudi 20 juin
- 9h30-17h30 Atelier de synthèse modulaire à l'institut Jean Le Rond ∂’Alembert avec Augustin Braud et Thomas Bottini

#### Vendredi 21 juin
- 14h Festival Les Fièvres musicales à la Pitié Salpêtrière dans les bâtiments et jardins de l’hôpital. Rencontre avec Fleur Cohen.
- 20h (facultatif) : concert (Libre comme) à la Chapelle Saint-Louis de la Pitié-Salpêtrière 

#### Lundi 24 juin
- 9h30-12h Atelier d'enregistrement sonore avec Hugues Genevois au LAM
- Déjeuner au LAM
- 14h-17h30 Atelier traitement du son avec Hugues Genevois

#### Mardi 25 juin
- 9h30-16h30 Travail autour des projets de stage à l'IReMus

#### Mercredi 26 juin
- 10h-12h Visite au Musée de l'Homme avec Susanne Fürniss

#### Jeudi 27 juin
- 9h30-17h30 Finalisation des projets de stage à l’IReMus

#### Vendredi 28 juin
- 10h Écoute d’une séance d’enregistrement dans le cadre du projet ERC Reach à l’IRCAM