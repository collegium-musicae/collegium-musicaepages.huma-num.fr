---
hide:
  - navigation
---

# Outils

<div class="grid cards" markdown>

- :simple-figma: Dessin vectoriel avec **Figma** <p>Figma est un logiciel de dessin vectoriel</p>
  [:simple-figma: Ouvrir Figma](https://www.figma.com/files/team/1302491504825543933/project/115311894/Stage-seconde?fuid=518882541974608923){ .md-button .md-button--primary }

- :simple-canva: PAO avec **Canva** <p>Canva est un logiciel de PAO</p>
  [:simple-canva: Ouvrir Canva](https://www.canva.com/brand/join?token=yWOwyH11wY0tzkbmVpWEMw&invitationDestinationType=group){ .md-button .md-button--primary }

</div>
