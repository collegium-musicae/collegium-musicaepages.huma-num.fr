# Déroulé de la séance

**Date :** Mercredi 19 juin 2024 | **Durée :** Demi-journée (4 heures) | **Lieu :** Datalab BnF

#### Objectif de l'atelier
Cet atelier vise à initier aux techniques de publication Assistée par Ordinateur (PAO) afin d'aider à concevoir et présenter un poster scientifique. À la fin de l'atelier, vous serez capables de structurer et de créer un poster visuellement attractif, ainsi que de le présenter de manière efficace.

#### Déroulement de la séance

**1. Introduction (15 minutes)**

- Accueil et présentation.
- Explication des objectifs de l'atelier.
- Description du programme de l'après-midi.

**2. Initiation au dessin vectoriel (30min)**

- Introduction aux concepts de base du dessin vectoriel.
- Prise en main de Figma et démonstration des fonctionnalités de base.

**3. Séance Pratique 1 - Création de Logo (1 heure)**

**Pause (15 minutes)**

**4. Initiation à la PAO (30min)**

- Introduction aux concepts de base de la PAO.
- Prise en main de Canva et démonstration des fonctionnalités de base.

**5. Séance Pratique 2 - Conception de Poster (1 heure)**

- Corrections et suggestions d'amélioration des posters.
- Travail sur la cohérence visuelle et la lisibilité.

**6. Techniques de Présentation Orale (15 minutes)**

- Principes de base d'une bonne présentation orale (clarté, concision, engagement).
- Exercice pratique : mini-présentations avec feedback constructif.

**7. Conclusion et Questions (15 minutes)**

- Récapitulatif des points clés de l'atelier.
