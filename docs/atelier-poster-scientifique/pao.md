# Publication assistée par ordinateur

### Identité de marque
!!! y "Exercice 1 : Importer un PDF et créer une identité visuelle"
    Importer un PDF dans Canva, puis extraire la police et la palette couleur.   


### Pratique

!!! y "Exercice 1 : Créer un portrait personnel"
    - **Première moitié :** Choix du sujet et structuration du poster (titre, sous-titres, sections principales).
    - **Deuxième moitié :** Ajout de contenu (texte, images, graphiques).

!!! y "Exercice 2 : Créer une affiche de colloque"
    
    Informations à renseigner sur le poster

    **Titre:** Représenter la musique ? Usages, fonctions, systèmes.

    **Sous-titre:** Différentes représentations pour différents usages.

    **Organisateur:** Le Collegium Musicae

    **Comité d'organisation:** Mathilde Callac, Hugo Pauget, Marco Fiorini.