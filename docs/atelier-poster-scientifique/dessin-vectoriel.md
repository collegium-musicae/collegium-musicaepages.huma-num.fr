# Dessin vectoriel

### 
Une image vectorielle possède un ratio ce qui forme un **repère** cartésien orthonormé (deux droites perpendiculaires qui se coupent au point d'origine).

Le repère contient des **noeuds** (nuage de points) définis par leurs coordonnées.

Les noeuds sont reliés par des **arcs**. Les arcs sont définis par des fonctions mathématiques.

Les arcs reliés forment des **zones de remplisssage**.

![alt text](../assets/image.png){ width="500" }

### Constructeurs de formes
Rectangle, Flèche, Ellipse, Polygone, Étoile.

### Outil pinceau
**1. Déplacer**

**2. Dessiner**

**3. Incurver**

**4. Remplir**
### Pratique

!!! y "Exercice 1 : Modifier un logo"
    Trouvez un logo au format SVG et modifiez sa couleur ou sa forme

!!! y "Exercice 2 : Vectoriser une image"
    Vectorisez collectivement à l'aide de l'outil pinceau dans Figma

    [:fontawesome-regular-image: Image](assets/dog.png){ .md-button .md-button--primary }
    
!!! y "Exercice 3 : Créer un logo"
    Écrivez votre prénom dans une zone de texte puis vectorisez le texte avec l'option *flatten*    
